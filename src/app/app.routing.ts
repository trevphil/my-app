import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { NowComponent } from './now/now.component';
import { ProjectsComponent } from './projects/projects.component';

const appRoutes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'about',
		component: AboutComponent
	},
	{
		path: 'contact',
		component: ContactComponent
	},
	{
		path: 'now',
		component: NowComponent
	},
	{
		path: 'projects',
		component: ProjectsComponent
	},
	{
		path: 'projects/:id',
		component: ProjectsComponent
	}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);