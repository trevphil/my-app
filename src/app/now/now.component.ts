import { Component } from '@angular/core';
import { ProjectsService, Project } from '../services/projects.service';

@Component({
  	selector: 'now',
  	templateUrl: './now.component.html',
	providers: [ ProjectsService ]
})

export class NowComponent  { 
	now: Project;
	service: ProjectsService;
	
	constructor(private projectsService: ProjectsService) {
		this.service = projectsService;
		this.projectsService.getNow().subscribe(projects => {
			if (projects.length > 0) {
				this.now = projects[0];
			}
		})
	}

}