import { Component } from '@angular/core';
import { ProjectsService, Project } from '../services/projects.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  	selector: 'projects',
  	templateUrl: './projects.component.html',
	providers: [ ProjectsService ]
})

export class ProjectsComponent  { 
	projects: Project[];
	selectedProject: Project;
	service: ProjectsService;
	selectedProjectID: number;
	
	constructor(private projectsService: ProjectsService, private route: ActivatedRoute) {
		this.service = projectsService;
		this.projectsService.getProjects().subscribe(projects => {
			if (projects.length > 0) {
				this.projects = projects;
				this.sortProjects();
				
				this.route.queryParams.subscribe(params => {
					this.selectedProjectID = Number(params['id']);
			
					if (this.selectedProjectID > 0) {
						for (let p of this.projects) {
							if (p.id == this.selectedProjectID) {
								this.selectedProject = p;
								break;
							}
						}
					}
				});
				
			}
		})
	}
	
	/*
	ngOnInit() {
		// Called AFTER the constructor, which is important...
		this.route.queryParams.subscribe(params => {
			this.selectedProjectID = Number(params['id']);
			
			if (this.selectedProjectID > 0) {
				console.log(this.projects);
			}
			
		});
	}
	*/
	
	sortProjects() {
		this.projects.sort((proj1, proj2): number => {
		  	if (proj1.id < proj2.id) {
		  		return 1;
			} 
			if (proj1.id > proj2.id) {
		    	return -1;
			}
		  	return 0;
		});
	}
	
	chose(project: Project) {
		this.selectedProject = project;
	}
}
