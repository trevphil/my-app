import { Component } from '@angular/core';
import { ProjectsService, Project } from '../services/projects.service';

@Component({
  	selector: 'about',
  	templateUrl: './about.component.html',
	providers: [ ProjectsService ]
})

export class AboutComponent  { 

	about: Project;
	service: ProjectsService;
	
	constructor(private projectsService: ProjectsService) {
		this.service = projectsService;
		this.projectsService.getAbout().subscribe(about => {
			this.about = about;
		})
	}
	
}
