import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class ProjectsService {

	constructor(private http: Http) { 
	}
	
	getAbout() {
		return this.http.get('json/about.json').map(res => res.json());
	}
	
	getNow() {
		return this.http.get('json/now.json').map(res => res.json());
	}

	getProjects() {
		return this.http.get('json/projects.json').map(res => res.json());
	}
	
	isImage(entry: string) {
		return entry.split(' ')[0] == 'IMG';
	}
	
	isLink(entry: string) {
		return entry.split(' ')[0] == 'LINK';
	}
	
	isText(entry: string) {
		return entry.split(' ')[0] == 'TEXT';
	}

	getImagePath(entry: string) {
		return entry.substring(4);
	}
	
	getLinkURL(entry: string) {
		return entry.split(' ')[1];
	}
	
	getLinkWord(entry: string) {
		var splitted = entry.split(' ');
		var discard = splitted[0].length + splitted[1].length + 2;
		return entry.substring(discard);
	}
	
	getText(entry: string) {
		return entry.substring(5);
	}

}

export interface Project {
	id: number;
	title: string;
	body: string[];
}