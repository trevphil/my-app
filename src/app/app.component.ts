// Making changes:

// Make the changes you want
// Test it out by entering 'npm start' in command-line, while in 'my-app' directory
// Then navigate to http://localhost:8080/

// Quit the server, then run 'npm run build' while in 'my-app' directory

// Remove '.DS_Store' files (in 'my-app' directory): find . -name '.DS_Store' -type f -delete

// Commit to git (BitBucket repo)

// Finally, upload through Mac OS X command line (while in 'dist' directory) using ncftp:
// ncftpput -R -v -u "ftp_username" -p "ftp_password" trevphil.com /public_html .

/*

.htaccess, if accidentally deleted:

RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

# not rewrite css, js and images
RewriteCond %{REQUEST_URI} !\.(?:css|js|map|jpe?g|gif|png)$ [NC]
RewriteRule ^(.*)$ /index.html?path=$1 [NC,L,QSA]

*/

import { Component } from '@angular/core';

@Component({
  	selector: 'my-app',
  	templateUrl: './app.component.html'
})

export class AppComponent  { }
