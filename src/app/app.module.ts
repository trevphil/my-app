import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { NowComponent } from './now/now.component';
import { ProjectsComponent } from './projects/projects.component';

import { routing } from './app.routing';

@NgModule({
	imports: [ BrowserModule, routing, HttpModule ],
	declarations: [ AppComponent, HomeComponent, AboutComponent, ContactComponent, NowComponent, ProjectsComponent ],
	bootstrap: [ AppComponent ]
})

export class AppModule { }

