// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

import 'rxjs';
import '@angularclass/hmr';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

import './style/gallery-grid-old-ie.css';
import './style/gallery-grid.css';
import './style/gallery-old-ie.css';
import './style/gallery.css';
import './style/styles.css';
